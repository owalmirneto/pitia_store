<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index');

    Route::group([ 'prefix' => 'admin' ], function() {
        Route::resource('products', 'Admin\ProductsController');
    });

    Route::group(['middleware' => ['api']], function () {
        Route::resource('products', 'Api\ProductsController', ['only' => ['index', 'show']]);
    });
});


