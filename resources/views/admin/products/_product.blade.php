<div class="col-md-4 col-xs-12 col-sm-6">
  <div class="form-group product text-center">
    <div class="row">
      <div class="col-md-12">
        <div>
          <div class="text-center thumbnail no-margin" style="margin-bottom:7px">
            <img src="{{ $product->image_url }}" alt="{{ $product->name }}" class="img-responsive" style="height:250px">
            <h5>
              <div>{{ $product->name }}</div>
              <small>R$ {{ $product->amount }} - {{ $product->released_date() }}</small>
            </h5>
          </div>

          <a class="btn btn-sm btn-success" href="{{ action('Admin\ProductsController@edit', $product->id) }}">
            <i class="fa fa-pencil"></i> editar
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
