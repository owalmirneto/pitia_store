<?php

namespace Tests\App\Http\Controllers\Api;

use App\Models\Product;
use App\Queries\ProductQuery;

/**
 * @group api_products
 */
class ProductsControllerTest extends \TestCase
{
    /**
     * setUp run before each test
     */
    public function setUp()
    {
        parent::setUp();

        Product::truncate();

        $this->query = new ProductQuery(new Product);
    }

    /**
     * tearDown run after each test
     */
    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @group api_products_index
     */
    public function testIndex()
    {
        # request
        $this->call('GET', action('Api\ProductsController@index'));
    }

    /**
     * @group api_products_index
     */
    public function testIndexWithOutProducts()
    {
        # request
        $crawler = $this->call('GET', action('Api\ProductsController@index'));
        $response = json_decode($crawler->getContent());

        # assertions
        $this->assertResponseOk();

        $this->assertTrue($response->empty);
        $this->assertTrue(!empty($response->message));
    }

    /**
     * @group api_products_index
     */
    public function testIndexWithProducts()
    {

        Product::create([
            'name' => $this->faker->name,
            'amount' => $this->faker->randomFloat()
        ]);

        # request
        $crawler = $this->call('GET', action('Api\ProductsController@index'));
        $response = json_decode($crawler->getContent());

        # assertions
        $this->assertResponseOk();

        $this->assertTrue(!$response->empty);
        $this->assertTrue(!empty($response->data));
    }

    /**
     * @group api_products_show
     */
    public function testShowWithOutProduct()
    {
        # request
        $crawler = $this->call('GET', action('Api\ProductsController@show', 1));
        $response = json_decode($crawler->getContent());

        # assertions
        $this->assertResponseOk();

        $this->assertTrue($response->empty);
        $this->assertTrue(!empty($response->message));
    }

    /**
     * @group api_products_show
     */
    public function testShowWithProduct()
    {
        $product = Product::create([
            'code' => str_random(13),
            'name' => $this->faker->name,
            'amount' => $this->faker->randomFloat()
        ]);

        # request
        $crawler = $this->call('GET', action('Api\ProductsController@show', $product->code));
        $response = json_decode($crawler->getContent());

        # assertions
        $this->assertResponseOk();

        $this->assertTrue(!$response->empty);
        $this->assertTrue(!empty($response->data));
    }
}
