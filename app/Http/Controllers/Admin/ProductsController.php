<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use App\Http\Controllers\Controller;
use App\Repositories\ProductRepository;

class ProductsController extends Controller
{
    public function __construct(ProductRepository $product)
    {
        $this->repository = $product;
    }

    public function index()
    {
        $products = $this->repository->all();

        return view('admin/products/index')->with('products', $products);
    }

    public function create()
    {
        $product = $this->repository->init();

        return view('admin/products/create')->with('product', $product);
    }

    public function store(ProductRequest $request)
    {
        $stored = $this->repository->store( $request->get('product') );

        if ($stored->success()) {
            return redirect(action('Admin\ProductsController@index'))
                ->with('success', $stored->message);
        }
        else {
            return redirect(action('Admin\ProductsController@index'))
                ->withInput()->withErrors($stored->errors);
        }
    }

    public function edit($id)
    {
        $product = $this->repository->find($id);

        return view('admin/products/edit')->with('product', $product);
    }

    public function update($id, ProductRequest $request)
    {
        $updated = $this->repository->update( $id, $request->get('product') );

        if ($updated->success()) {
            return redirect(action('Admin\ProductsController@index'))
                ->with('success', $updated->message);
        }
        else {
            return redirect(action('Admin\ProductsController@index'))
                ->withInput()->withErrors($updated->errors);
        }
    }
}
