var Initializer = {
  init: function () {
    Datepicker.init();
    MeioMask.init();
  }
};

$(document).on('ready', function () { Initializer.init(); });
