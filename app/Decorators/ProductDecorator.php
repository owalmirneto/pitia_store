<?php

namespace App\Decorators;

trait ProductDecorator
{
    public function released_date($format = 'd/m/Y')
    {
        return isset($this->released_at) ? $this->released_at->format($format) : null;
    }

    public function amount_to_currency()
    {
        return 'R$ ' . number_format($this->amount, 2, ',', '.');
    }

    public function handler()
    {
        return (object) [
            'code' => $this->code,
            'name' => $this->name,
            'image_url' => $this->image_url,
            'amount' => $this->amount_to_currency(),
            'released_date' => $this->released_date(),

        ];
    }

    public function getImageUrlAttribute($image_url)
    {
        if (!empty($image_url)) {
            return substr($image_url, 0, 4) != 'http' ? \URL::to($image_url) : $image_url;
        }
    }
}
