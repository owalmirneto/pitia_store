{!! Form::model($product, $form_attributes) !!}

  <div class="form-group">
    <div class="row">
      <div class="col-sm-12">
        {!! Form::label('product[name]', 'Nome') !!}
        {!! Form::text('product[name]', $product->name, [ 'class' => 'form-control input-lg' ]) !!}
      </div>
    </div>
  </div>

  <div class="form-group">
    <div class="row">
      <div class="col-sm-3">
        {!! Form::label('product[amount]', 'Valor') !!}
        {!! Form::text('product[amount]', $product->amount, [ 'class' => 'form-control', 'data-mask' => 'numeric' ]) !!}
      </div>

      <div class="col-sm-6">
        {!! Form::label('product[image_url]', 'Link da imagem') !!}
        {!! Form::text('product[image_url]', $product->image_url, [ 'class' => 'form-control' ]) !!}
      </div>

      <div class="col-sm-3">
        {!! Form::label('product[released_at]', 'Data de lançamento') !!}
        {!! Form::text('product[released_at]', $product->released_date(), [ 'class' => 'form-control bs-datepicker', 'data-mask' => 'date']) !!}
      </div>
    </div>
  </div>

  <div class="form-group">
    {!! Form::submit('Salvar produto', [ 'class' => 'btn btn-primary btn-block' ]) !!}
  </div>
{!! Form::close() !!}
