# Pitia Store

This is an application test for [Pitia](http://pitia.com.br)

## Installation

#### Back-end

```bash
[sudo] composer install
[sudo] cp .env.example .env
php artisan key:generate
```

Set your environment file `.env`

#### Database Application

Create database: `pitia_store_development`

```bash
php artisan migrate
```

#### Front-end

```bash
[sudo] npm install
[sudo] chmod 777 -R vendor && bower update
```

#### Run application

```bash
php artisan serve
```

**Optinal Seed database:**

You should create folder `/public/images/products` only then run `php artisan db:seed`

Or you can create product in: http://localhost:8000/admin/products/create

#### Tests

Create database:  `pitia_store_test`

```bash
php artisan migrate --database=mysql_testing
vendor/bin/phpunit
```
