<div class="row">
  <div class="col-md-12">
    @if (Session::has('status'))
      <div class="alert alert-success">
        {{ Session::get('status') }}
      </div>
    @endif

    @if(Session::has('success'))
      <div class="alert alert-success">
        <strong>Sucesso! </strong>  {!! Session::get('success') !!}
      </div>
    @endif

    @if(Session::has('danger'))
      <div class="alert alert-danger">
        <strong>Erro! </strong>  {!! Session::get('danger') !!}
      </div>
    @endif

    @if(Session::has('error'))
      <div class="alert alert-danger">
        <strong>Erro! </strong>  {!! Session::get('error') !!}
      </div>
    @endif

    @if(Session::has('info'))
      <div class="alert alert-info">
        <strong>Informação! </strong>  {!! Session::get('info') !!}
      </div>
    @endif

    @if(Session::has('warning'))
      <div class="alert alert-warning">
        <strong>Atenção! </strong>  {!! Session::get('warning') !!}
      </div>
    @endif

    @if ($errors->any())
      <ul class="alert alert-danger">
        <li><strong>Erro!</strong></li>

        @foreach($errors->all() as $key => $error)
          <li>* {{ $error }}</li>
        @endforeach
      </ul>
    @endif
  </div>
</div>
