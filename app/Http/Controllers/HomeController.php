<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ProductRepository;

class HomeController extends Controller
{
    public function __construct(ProductRepository $product)
    {
        $this->repository = $product;
    }

    public function index()
    {
        $products = $this->repository->all();

        return view('home/index')->with('products', $products);
    }
}
