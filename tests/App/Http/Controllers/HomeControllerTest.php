<?php

namespace Tests\App\Http\Controllers;

/**
 * @group home
 */
class HomeControllerTest extends \TestCase
{
    /**
     * setUp run before each test
     */
    public function setUp()
    {
        parent::setUp();
    }

    /**
     * tearDown run after each test
     */
    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @group home_index
     */
    public function testIndex()
    {
        # request
        $this->call('GET', action('HomeController@index'));

        # assertions vars
        $this->assertViewHas(['products']);

        # assertions
        $this->assertResponseOk();
    }
}
