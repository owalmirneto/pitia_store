@extends(config('layout.default'))

@section('content')
<div class="container">
  <h1>Produtos</h1>

  @unless ($products->isEmpty())
    <div class="row">
      @foreach ($products as $key => $product)
        @include('admin/products/_product', ['product' => $product])
      @endforeach
    </div>
  @endif
</div>
@endsection
