@extends('layouts/master')

@section('head')
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{{ config('layout.app_name') }}</title>

  <link href="{{ asset('/stylesheets/application.css') }}" rel="stylesheet" />

  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
@stop

@section('nav')
  <nav id="navigation" class="navbar navbar-inverse" style="border-radius: 0;">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a href="{{ action('HomeController@index') }}">
          <img id="logo" src="{{ asset('images/logo.png') }}" style="height:47px" alt="{{ config('layout.app_name') }}" />
        </a>
      </div>

      <ul class="nav navbar-nav">
        <li>
          <a href="{{ action('Admin\ProductsController@index') }}">
            <i class="fa fa-bars"></i> produtos
          </a>
        </li>
        <li>
          <a href="{{ action('Admin\ProductsController@create') }}">
            <i class="fa fa-plus"></i> adicionar produto
          </a>
        </li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
        <li>
          <a href="#">
            <i class="fa fa-sign-in fa-fw"></i> Entrar
          </a>
        </li>
      </ul>
    </div>
  </nav>
@stop

@section('container')
  <div class="container-fluid">
    @include('shared/_flash_messages')
  </div>

  @yield('content')
@stop

@section('footer')
  <hr />
  <footer class="container-fluid">
    <div class="row">
      <div class="col-md-6">{{ config('layout.app_name') }} © {{ date('Y') }}</div>
      <div class="col-md-6 text-right">
        <a href="http://wfsneto.com.br" target="blank">by wfsneto</a>
      </div>
    </div>
  </footer>
@stop

@section('scripts')
  {{-- javascripts --}}
  <script src="{{ asset('/javascripts/application.js') }}"></script>
@stop
