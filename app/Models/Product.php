<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends BaseModel
{
    use \App\Decorators\ProductDecorator;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'amount', 'image_url', 'released_at',
    ];

    protected $dates = [
        'released_at',
    ];
}
