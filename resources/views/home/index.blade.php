@extends(config('layout.default'))

@section('content')
<div class="container-fluid">
  <div class="text-center">
    <div class="form-group">
      <h1>Pitia Store <small>Seja bem Vindo</small></h1>
    </div>

    <a class="btn btn-lg btn-default" href="{{ action('Admin\ProductsController@index') }}">
      <i class="fa fa-bars"></i> Ver todos o produtos
    </a>

    <a class="btn btn-lg btn-default" href="{{ action('Admin\ProductsController@create') }}">
      <i class="fa fa-plus"></i> Adicionar um produto
    </a>
  </div>
</div>
@endsection
