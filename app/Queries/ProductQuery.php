<?php
namespace App\Queries;

use App\Models\Product;

class ProductQuery
{
    private $model;

    public function __construct(Product $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        $products = $this->model->get();

        $collection = [];
        if (!$products->isEmpty()) {
            foreach ($products as $key => $product) {
                $collection[] = $product->handler();
            }
        }
        return $collection;
    }

    public function find($code)
    {
        $product = $this->model->where('code', $code)->first();

        if ($product) return $product->handler();
    }
}
