<?php

namespace Tests\App\Http\Controllers\Admin;

use App\Models\Product;
use App\Repositories\ProductRepository;

/**
 * @group products
 */
class ProductsControllerTest extends \TestCase
{
    /**
     * setUp run before each test
     */
    public function setUp()
    {
        $this->repository = new ProductRepository(new Product);

        parent::setUp();

        $this->data['product'] = [
            'name' => $this->faker->name,
            'amount' => $this->faker->randomFloat(),
            'image_url' => $this->faker->url,
            'released_at' => date('d/m/Y'),
        ];
    }

    /**
     * tearDown run after each test
     */
    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @group products_index
     */
    public function testIndex()
    {
        # request
        $this->call('GET', action('Admin\ProductsController@index'));

        # assertions vars
        $this->assertViewHas(['products']);

        # assertions
        $this->assertResponseOk();
    }

    /**
     * @group products_create
     */
    public function testCreate()
    {
        # request
        $this->call('GET', action('Admin\ProductsController@create'));

        # assertions vars
        $this->assertViewHas(['product']);

        # assertions
        $this->assertResponseOk();
    }

    /**
     * @group products_store
     */
    public function testStore()
    {
        # request
        $this->call('POST', action('Admin\ProductsController@store'), $this->data);

        # finder created
        $product = $this->repository->eloquent->orderBy('id', 'desc')->first();

        # assertions
        $this->assertResponseStatus(302);
        $this->assertRedirectedToAction('Admin\ProductsController@index');
        $this->assertEquals($product->name, $this->data['product']['name']);

        # remove
        $this->repository->eloquent->where('id', '>', 0)->forceDelete();
    }

    /**
     * @group products_edit
     */
    public function testEdit()
    {
        # persist
        $product = $this->repository->eloquent->create($this->data);

        # request
        $this->call('GET', action('Admin\ProductsController@edit', $product->id));

        # assertions vars
        $this->assertViewHas(['product']);

        # assertions
        $this->assertResponseOk();
    }

    /**
     * @group products_update
     */
    public function testUpdate()
    {
        # persist
        $product = $this->repository->eloquent->create($this->data);

        # params
        $this->data['product']['name'] =  'Name updated';

        # request
        $this->call('PUT', action('Admin\ProductsController@update', $product->id), $this->data);

        # assertions
        $this->assertResponseStatus(302);
        $this->assertRedirectedToAction('Admin\ProductsController@index');
        $this->assertEquals($this->repository->find($product->id)->name, $this->data['product']['name']);

    }
}
