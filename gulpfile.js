var elixir = require('laravel-elixir');

var assets_path = 'resources/assets/';
var bower_path = 'vendor/bower_components/';

elixir(function(mix) {
  mix

  // Fonts
  .copy(bower_path + '/font-awesome/fonts', 'public/fonts/font-awesome')

  // javascripts
  .scripts([
    // Vendors
    bower_path + 'jquery/dist/jquery.min.js',
    bower_path + 'bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
    bower_path + 'jquery-meiomask/dist/meiomask.min.js',

    // Application
    assets_path + 'javascripts/initializer.js',
    assets_path + 'javascripts/vendors/meiomask.js',
    assets_path + 'javascripts/vendors/datepicker.js',
  ],
  'public/javascripts/application.js', './')

  // stylesheets
  .less('application.less', 'public/stylesheets/application.css');
});
