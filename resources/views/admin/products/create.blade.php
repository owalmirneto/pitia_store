@extends(config('layout.default'))

@section('content')
<div class="container">
  <h3>Produtos <small>novo</small></h3> <hr />

  <div class="page-content">
    <div class="panel">
      <div class="panel-body">
        @include('admin/products/_form', ['form_attributes' => [
          'url' => action('Admin\ProductsController@store'),
          'method' => 'POST'
        ]])
      </div>
    </div>
  </div>
</div>
@endsection
