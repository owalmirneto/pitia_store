<?php
namespace App\Http\Controllers\Api;

header("Access-Control-Allow-Origin: *");

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Queries\ProductQuery;

class ProductsController extends Controller
{
    public function __construct(ProductQuery $product)
    {
        $this->query = $product;
    }

    public function index()
    {
        $products = $this->query->all();

        if (empty($products)) {
            return [ 'empty' => true, 'message' => 'Nenhum produto foi encontrado' ];
        }
        else {
            return [ 'empty' => false, 'data' => $products ];
        }
    }

    public function show($id)
    {
        $product = $this->query->find($id);

        if (empty($product)) {
            return [ 'empty' => true, 'message' => 'Produto não encontrado' ];
        }
        else {
            return [ 'empty' => false, 'data' => $product ];
        }
    }
}
