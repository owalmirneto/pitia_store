var MeioMask = {
  init: function () {
    this.extras();
    this.simple();
  },

  simple: function () {
    $('input[data-mask]').each(function() {
      var input = $(this);
      input.setMask( input.data('mask') );
    });
  },

  extras: function () {
    $.mask.rules.n = /[0-9]?/

    $.mask.masks['phone-br'] = { mask : '(99) 9999-9999n' };
    $.mask.masks['numeric'] = { mask: '99.999999999', type: 'reverse', defaultValue: '000' };
    $.mask.masks['percent'] = { mask : '199' };
  }
};
