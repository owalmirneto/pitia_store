<?php
namespace App\Repositories;

use App\Models\Product;

class ProductRepository extends BaseRepository
{
    public $name = 'produto';
    public $gender = 'o';

    public function __construct(Product $eloquent)
    {
        $this->eloquent = $eloquent;
    }

    public function beforeSave($attributes)
    {
        $this->eloquent->code = str_random(13);

        $attributes['released_at'] = implode('-', array_reverse(explode('/', $attributes['released_at'])));

        return $attributes;
    }
}
