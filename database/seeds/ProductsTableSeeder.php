<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($i=0; $i < 6; $i++) {
            $image_url = $faker->image(public_path('images/products'), 340, 250, 'food');

            DB::table('products')->insert([
                'name' => $faker->words(5, true),
                'code' => str_random(13),
                'amount' => $faker->randomFloat(2, 10, 50),
                'image_url' => str_replace(public_path(), '', $image_url),
                'released_at' => $faker->dateTimeBetween('-1 Month', '1 Month'),
            ]);
        }
    }
}
