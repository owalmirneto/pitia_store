<!doctype html>
<html lang="en">
<head>
  @yield('head')
  @yield('stylesheets')
</head>
<body class="@yield('class-body')">
  @yield('nav')

  @yield('container')

  @yield('footer')

  @yield('scripts')
  @yield('javascripts')
</body>
</html>
